package org.ubfs.word.temp.annoation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.ubfs.word.temp.constant.ExcelType;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public  @interface ExcelField {
	
	int type() default ExcelType.TEXT;
	
	int startRow() default 0;
	

}
