package org.ubfs.word.temp.beans;

import lombok.Data;

@Data
public class ExcelModel {
	
  private String text;
  private int merge;
  
  public ExcelModel(String text, int merge) {
		this.text = text;
		this.merge = merge;
  }
  
}
