package org.ubfs.word.temp.tool;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.util.Assert;
import org.ubfs.word.temp.beans.BaseFileTemp;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class ExcelBase {

	protected Sheet getSheet(int sheetNum, InputStream in) {
		Workbook workbook = null;
		try {
			workbook = WorkbookFactory.create(in);
			return workbook.getSheetAt(sheetNum);
		} catch (Exception e) {
			try {
				if (workbook != null) {
					workbook.close();
				}
			} catch (IOException e1) {
				throw new RuntimeException(e1.getMessage());
			}
			return getHssfSheet(sheetNum, in);
		}
	}

	@SuppressWarnings("resource")
	private Sheet getHssfSheet(int sheetNum, InputStream in) {
		try {
			HSSFWorkbook workbook = new HSSFWorkbook(in);
			return workbook.getSheetAt(sheetNum);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	/**
	 * 格式校验
	 * 
	 * @author taolq
	 * @date 2020年5月14日
	 * @time 下午2:16:42
	 * @param in
	 */
	protected BufferedInputStream fromatVaildata(InputStream in) {
		BufferedInputStream inputStream = new BufferedInputStream(in);
		boolean isExcel = FileVaildata.isExcel(inputStream);
		Assert.isTrue(isExcel, "请使用Excel格式文件作为模板");
		return inputStream;
	}

	/**
	 * 导出 word
	 * 
	 * @param document
	 * @param t
	 */
	protected void writeNewFile(Workbook workbook, BaseFileTemp base) {
		FileOutputStream fileOut = null;
		try {
			log.info("----------------------------------------------");
			log.info("准备开始导出替换后的excel 文件");
			File folder = new File(getFolderByFilePath(base.getOutPath()));
			if (!folder.exists()) {
				log.info("导出文件夹路径不存在，开始自动创建：{}", folder);
				folder.mkdirs();
			}
			log.info("导出文件路径：{}", base.getOutPath());
			fileOut = new FileOutputStream(new File(base.getOutPath()));
			workbook.write(fileOut);
			fileOut.flush();
			log.info("excel文件导出成功");
		} catch (Exception e) {
			log.error("excel 文件导出异常", e);
			throw new RuntimeException("文件导出时发生异常", e);
		} finally {
			try {
				fileOut.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * 从文件路径中获取文件夹路径
	 * 
	 * @param filePath
	 * @return
	 */
	private String getFolderByFilePath(String filePath) {
		try {
			filePath = filePath.replace("\\", "/");
			String[] split = filePath.split("\\/");
			String folder = filePath.substring(0, filePath.indexOf(split[split.length - 1]));
			return folder;
		} catch (Exception e) {
			throw new RuntimeException("获取文件夹出现错误", e);
		}
	}

	/**
	 * 获取单元格合并数
	 * 
	 * @param cell
	 * @param sheet
	 * @return
	 */
	protected int GetMergeCell(Cell cell, Sheet sheet) {
		int mergeSize = 1;
		List<CellRangeAddress> mergedRegions = sheet.getMergedRegions();
		for (CellRangeAddress cellRangeAddress : mergedRegions) {
			if (cellRangeAddress.isInRange(cell)) {
				// 获取合并的行数
				mergeSize = cellRangeAddress.getLastColumn() - cellRangeAddress.getFirstColumn() + 1;
				break;
			}
		}
		return mergeSize;
	}

	/**
	 * 通用样式
	 * 
	 * @param workbook
	 * @return
	 */
	protected CellStyle getTopColumnStyle(Workbook workbook) {
		CellStyle cellStyle = workbook.createCellStyle();
		// 设置字体
		Font font = workbook.createFont();
		// 设置字体大小
		font.setFontHeightInPoints((short) 12);
		// 设置字体名字
		font.setFontName("Courier New");
		// 在样式用应用设置的字体;
		cellStyle.setFont(font);
		// 设置自动换行;
		cellStyle.setWrapText(false);
		// 设置水平对齐的样式为居中对齐;
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		// 设置垂直对齐的样式为居中对齐;
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		// 设置单元格背景颜色
		cellStyle.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());

		return cellStyle;
	}

	/*
	 * 列数据信息单元格样式
	 */
	protected CellStyle getBodyStyle(Workbook workbook) {
		CellStyle style = getTopColumnStyle(workbook);   
		style.setWrapText(true);      
		this.setCellBorder(style);
		return style;
	}

	/**
	 * 设置边框
	 * 
	 * @param style
	 * @return
	 */
	private CellStyle setCellBorder(CellStyle style) {
		style.setBorderBottom(BorderStyle.THIN);// 下边框
		style.setBorderLeft(BorderStyle.THIN);// 左边框
		style.setBorderRight(BorderStyle.THIN);// 右边框
		style.setBorderTop(BorderStyle.THIN); // 上边框
		return style;
	}
	
	
	/**
	 * excel删除行
	 * @param sheet   sheet  
	 * @param startRow 起始行
	 * @param endRow   结束行
	 */
	protected void deleteRow(Sheet sheet,int startRow,int endRow){
		for(int i = startRow;i<startRow+endRow-1;i++){
			//此方法只能删除行内容
			sheet.removeRow(sheet.getRow(i));;
		}
		int lastRowNum = sheet.getLastRowNum();
		//此方法是将余下的行向上移
		sheet.shiftRows(endRow, lastRowNum, (startRow-endRow));
	}

}
