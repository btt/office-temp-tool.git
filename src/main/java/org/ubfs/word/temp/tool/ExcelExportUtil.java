package org.ubfs.word.temp.tool;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.ubfs.word.temp.annoation.ExcelField;
import org.ubfs.word.temp.beans.BaseFileTemp;
import org.ubfs.word.temp.beans.ExcelModel;
import org.ubfs.word.temp.constant.ExcelType;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExcelExportUtil extends ExcelBase {
	
	/**
	 * 导出execl方法
	 * @param t
	 * @return
	 */
	public String  export(BaseFileTemp t){
		try {
			Assert.notNull(t, "模板实体bean不能为空");
			log.info("excel 模板文件路径：{}",t.getTempPath());
			t.vaildata();
			fromatVaildata(new FileInputStream(new File(t.getTempPath())));
			Sheet sheet = this.getSheet(0, new FileInputStream(new File(t.getTempPath())));
			for (int rowNum = 0; rowNum < sheet.getLastRowNum(); rowNum++) {
				Row row = sheet.getRow(rowNum);
				short cellNum = row == null ? 0 : row.getLastCellNum();
				for (int i = 0; i < cellNum; i++) {
					Cell cell = row.getCell(i);
					if(cell != null) 
						this.paramForEntity(t, cell,rowNum,sheet);
				}
			}
			//导出新的文件
			this.writeNewFile(sheet.getWorkbook(), t);
			return t.getOutPath();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	/**
	 * 匹配并替换变量
	 * @param field
	 * @param input
	 * @param value
	 * @return
	 */
	private String matchAndReplaceText(String field,String input,Object value) {
		Pattern compile = Pattern.compile("\\$\\{("+field+")\\}");
		Matcher matcher = compile.matcher(input);
		String text = value == null ? "" : value.toString();
		return matcher.find() ? matcher.replaceAll(text) : input;
	}
	private void paramForEntity(BaseFileTemp entity,Cell cell, int rowIndex, Sheet sheet) {
		try {
			String cellValue = cell.getStringCellValue();
			if(!StringUtils.hasText(cellValue)) {
				return;
			}
			Field[] fields = entity.getClass().getDeclaredFields();
			for(Field filed : fields) {
				ExcelField annotation = filed.getAnnotation(ExcelField.class);
				if(null == annotation) {
					return ;
				}
				//文本赋值
				if(annotation.type() == ExcelType.TEXT) {
					filed.setAccessible(true);
					cellValue = this.matchAndReplaceText(filed.getName(),cellValue,filed.get(entity));
				}
				//循环列表
				if(annotation.type() == ExcelType.LIST) {
					filed.setAccessible(true);
					this.matchAndReplaceList(annotation,filed.getName(),cellValue,filed.get(entity),rowIndex,sheet);
					if(this.matchName(filed.getName(), cellValue)) {
						cellValue = "";
					}
				}
			}
			cell.setCellValue(cellValue);
		} catch (Exception e) {
			log.error("paramForEntity===>",e);
			throw new RuntimeException(e);
		}
	}
	/**
	 * 匹配数据列表
	 * @param annotation 
	 * @param name
	 * @param cellValue
	 * @param object
	 * @param rowIndex
	 * @param sheet 
	 */
	@SuppressWarnings("unchecked")
	private void matchAndReplaceList(ExcelField annotation, String name, String cellValue, Object object, int rowIndex, Sheet sheet) {
		// TODO Auto-generated method stub
		try {
			boolean matches = Pattern.matches("\\$\\{("+name+")\\}",cellValue);
			if(!matches) {
				return ;
			}
			int startRow = annotation.startRow() + rowIndex;
			Row labelRow = sheet.getRow(startRow);
			Row sourceRow = sheet.getRow(startRow + 1);
			if(object != null && object instanceof List) {
				List<Object> list = (List<Object>) object;
				if(sourceRow != null )//自动下移末尾行
					sheet.shiftRows(sourceRow.getRowNum(), sourceRow.getRowNum()+list.size(), list.size()-1);
				//匹配并填充实体
				for (int rowNum = startRow; rowNum < startRow + list.size(); rowNum++) {
					this.setTextForEntity(list.get(rowNum - startRow),labelRow,rowNum,sheet);
				}
			}
		} catch (Exception e) {
			log.error("matchAndReplaceList===>",e);
			throw new RuntimeException(e);
		}
	}
	/**
	 * 填充内容
	 * @param data
	 * @param labelRow
	 * @param rowNum
	 * @param sheet
	 */
	private void setTextForEntity(Object data, Row labelRow,int rowNum, Sheet sheet) {
		// TODO Auto-generated method stub
		try {
			List<CellRangeAddress> arrayList = new ArrayList<CellRangeAddress>();
			short cellNum = labelRow == null ? 0 : labelRow.getLastCellNum();
			Row createRow = sheet.createRow(rowNum);
			createRow.setHeight((short)500);
			Field[] fields = data.getClass().getDeclaredFields();
			for(int i =0;i<cellNum;i++) {
				  for(Field field : fields) {
					boolean matchKey = this.matchKey(field.getName(), labelRow.getCell(i).getStringCellValue());
					if(matchKey) {
						field.setAccessible(true);
						Object object = field.get(data);
						Cell createCell = createRow.createCell(i);
						createCell.setCellStyle(this.getBodyStyle(sheet.getWorkbook()));
						if(object instanceof ExcelModel) {
							ExcelModel em = (ExcelModel) object;
							createCell.setCellValue(em.getText());
							//合并单元格  ==================>核心代码
							if(em.getMerge() > 0) {
								/**
								 * (int firstRow, int lastRow, int firstCol, int lastCol)
								 * 
								 * firstRow 起始行（纵向）
								 * lastRow 截止行（纵向）
								 * firstCol 起始列（横向）
								 * lastCol 截止列（横向）
								 * 
								 */
								CellRangeAddress region = new CellRangeAddress(rowNum, rowNum + em.getMerge()-1, i, i);
								arrayList.add(region);
							}
						}else {
							createCell.setCellValue(object==null ? "" : object.toString());
						}
						
					}
				}
				  //sheet.autoSizeColumn((short)i); //调整列宽度
			}
		  //添加合并单元格的信息
          arrayList.forEach(region ->{
        	  sheet.addMergedRegion(region);
          });
		} catch (Exception e) {
			log.error("填充数据发生错误",e);
			throw new RuntimeException(e);
		}
		
	}
	
	private boolean matchKey(String key,Object text) {
		String value = text==null ? "" : text.toString();
		return Pattern.matches("\\$\\<("+key+")\\>",value);
	}
	
	private boolean matchName(String key,Object text) {
		String value = text==null ? "" : text.toString();
		return Pattern.matches("\\$\\{("+key+")\\}",value);
	}
	
	
	
	

}
