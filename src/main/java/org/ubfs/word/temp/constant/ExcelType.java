package org.ubfs.word.temp.constant;

/**
 * word field type
 * @author taolongqing
 *
 */
public class ExcelType {
   /**
    * 文本
    */
   public final static int TEXT = 1;
   
   /**
    * 列表
    */
   public final static int LIST = 2;
	
}
