package org.ubfs.word.temp.constant;

/**
 * word field type
 * @author taolongqing
 *
 */
public class WordParamsType {
   /**
    * 文件
    */
   public final static int FILE = 1;
   /**
    * 文本
    */
   public final static int TEXT = 2;
   
   /**
    * 列表
    */
   public final static int LIST = 3;
	
}
