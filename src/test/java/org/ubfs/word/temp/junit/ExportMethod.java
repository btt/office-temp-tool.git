package org.ubfs.word.temp.junit;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.ubfs.word.temp.beans.ExcelModel;
import org.ubfs.word.temp.tool.ExcelExportUtil;

import junit.framework.TestCase;

public class ExportMethod extends TestCase {
	
	private ExportTest exportTest;
	
	List<ArchInfo> archInfoList = new ArrayList<ArchInfo>();
	
	@Before
	public void init() {
		exportTest = new ExportTest(); 
		exportTest.setOrganName("测试机构");
		exportTest.setPartYear("2020");
		
		ArchInfo archInfo = new ArchInfo();
		archInfo.setLabelName("培训报告");
		archInfo.setParentLabelName(new ExcelModel("工作报告",3));
		archInfo.setFileName("11.txt");
		archInfo.setPageNum("12");
		archInfo.setOrder("1");
		archInfo.setUploadDate("2018-12-12");
		archInfo.setRecieveUser("赵雄");
		
		ArchInfo archInfo2 = new ArchInfo();
		archInfo2.setLabelName("宣传报告");
		archInfo2.setParentLabelName(new ExcelModel("工作报告",0));
		archInfo2.setFileName("22.txt");
		archInfo2.setPageNum("12");
		archInfo2.setOrder("2");
		archInfo2.setUploadDate("2018-12-12");
		archInfo2.setRecieveUser("赵雄");
		
		ArchInfo archInfo3 = new ArchInfo();
		archInfo3.setLabelName("宣传报告");
		archInfo3.setParentLabelName(new ExcelModel("内控报告",0));
		archInfo3.setFileName("22.txt");
		archInfo3.setPageNum("12");
		archInfo3.setOrder("3");
		archInfo3.setUploadDate("2018-12-12");
		archInfo3.setRecieveUser("赵雄");
		archInfoList.add(archInfo);
		archInfoList.add(archInfo2);
		archInfoList.add(archInfo3);
		exportTest.setArchInfoList(archInfoList);
		
		exportTest.setTempPath("F:\\银雁科技\\项目资料\\海口人行\\档案管理需求\\11模板.xls");
		exportTest.setOutPath("d://123.xls");
	}
	
	@Test
	public void testMethod() {
		this.init();
		new ExcelExportUtil().export(exportTest);
	}

}
