package org.ubfs.word.temp.junit.execlparse;

import org.apache.ibatis.jdbc.SQL;
import org.junit.Test;
import org.ubfs.word.temp.tool.ExeclParseUtil;
import xin.xihc.utils.common.DateUtil;

import java.io.*;
import java.util.List;

public class ExeclParseTester {




    public static String toSerialHex(Object obj) throws IOException {
        ByteArrayOutputStream bos = null;
        try {
             bos = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(bos);
            os.writeObject(obj);
            os.flush();
            os.close();
            byte[] byteArray = bos.toByteArray();
            String hexString = getHexString(byteArray);
            return hexString;
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            bos.close();
        }
        return null;
    }

    public static byte[] hexToByteArray(String inHex){
        int hexlen = inHex.length();
        byte[] result;
        if (hexlen % 2 == 1){
            //奇数
            hexlen++;
            result = new byte[(hexlen/2)];
            inHex="0"+inHex;
        }else {
            //偶数
            result = new byte[(hexlen/2)];
        }
        int j=0;
        for (int i = 0; i < hexlen; i+=2){
            result[j]= hexToByte(inHex.substring(i,i+2));
            j++;
        }
        return result;
    }

    public static byte hexToByte(String inHex){
        return (byte)Integer.parseInt(inHex,16);
    }

    public static String getHexString(byte[] byteArray){
        StringBuffer sb = new StringBuffer();
        for(byte b :  byteArray){
            int v = b & 0XFF;
            String hv = Integer.toHexString(v);
            if(hv.length() < 2){
                sb.append(0);
            }
            sb.append(hv);
        }
        return sb.toString();
    }



    /**
     * 解析 execl 中的内容
     * @param
     * @throws IOException
     */
    @Test
    public void  parseExecl() throws IOException {
        ExeclParseUtil parseUtil = new ExeclParseUtil();
        File file = new File("F:\\银雁科技\\项目资料\\金科项目\\代收付\\资料\\新建 XLS 工作表.xls");
        FileInputStream fileInputStream = new FileInputStream(file);
        List<FeeTermDict> dataList = parseUtil.getDataList(fileInputStream, FeeTermDict.class);
        System.out.println("读取内容：" + dataList);
    }

    /**
     * 序列化测试
     */
    @Test
    public void testMethod() {
        try {
            ExeclParseUtil parseUtil = new ExeclParseUtil();
            File file = new File("F:\\银雁科技\\项目资料\\金科项目\\代收付\\资料\\新建 XLS 工作表.xls");
            FileInputStream fileInputStream = new FileInputStream(file);
            List<FeeTermDict> dataList = parseUtil.getDataList(fileInputStream, FeeTermDict.class);
            //序列化
            String hexString = toSerialHex(dataList);
            // 从字节数组中反序列化对象
            ByteArrayInputStream bis = new ByteArrayInputStream(hexToByteArray(hexString));
            ObjectInputStream in = new ObjectInputStream(bis);
            List<FeeTermDict>  readUserInfo = (List<FeeTermDict> ) in.readObject();
            System.out.println("序列化结果：" + readUserInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 转sql 测试
     */
    @Test
    public void toSql() throws FileNotFoundException {
        ExeclParseUtil parseUtil = new ExeclParseUtil();
        File file = new File("F:\\银雁科技\\项目资料\\金科项目\\代收付\\资料\\新建 XLS 工作表.xls");
        FileInputStream fileInputStream = new FileInputStream(file);
        List<FeeTermDict> dataList = parseUtil.getDataList(fileInputStream, FeeTermDict.class);

        dataList.stream().forEach(x ->{
            String sql = insertPersonSql(x);
            System.out.println(sql+";");
        });
    }

    public String insertPersonSql(FeeTermDict dict) {
        return new SQL() {{
               INSERT_INTO("fee_term_dict")
              .VALUES("code",String.format("\"%s\"",dict.getCode()))
              .VALUES("name",String.format("\"%s\"",dict.getName()))
              .VALUES("type",String.format("\"%s\"",dict.getType()))
              .VALUES("create_time",String.format("\"%s\"",DateUtil.getNow()))
              .VALUES("update_time","null");
        }}.toString();
    }


}
