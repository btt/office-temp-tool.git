package org.ubfs.word.temp.junit.convertPdf;

import xin.xihc.utils.common.FileUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import org.ubfs.word.temp.junit.generator.HtmlGenerator;
import org.ubfs.word.temp.junit.generator.PdfGenerator;

public class Junit01 {

    public static void main(String[] args) throws Exception {
        Map<String, Object> data = new HashMap<>();
        data.put("userName", "孙悟空");
        data.put("userNum", "110101199003074071");
        data.put("year", "2022");
        data.put("month", "01");
        data.put("day", "24");
        data.put("siteName", "帅链平台");
        data.put("ticketTitle", "帅票");
        data.put("tenEntName", "郑州市公共交通集团有限公司");
        data.put("indexTitle", "帅链科技");
      /*  String s = FreemarkerUtil.loadTemplateFromFile("das-html2pdf.htm", data);
        System.out.println(s);*/

        /*String str = FileUtil.readFileToStr(new File("E:\\JAVA\\workspace\\湖南谷歌地图项目\\工单\\template\\das-html2pdf.htm"));
        File file = new File("E:\\JAVA\\workspace\\湖南谷歌地图项目\\工单\\template\\result.pdf");
        HtmlUtil.createPDFFromHtml(s,file.getPath());*/

       // HtmlUtil2.html2Pdf("E:\\JAVA\\workspace\\湖南谷歌地图项目\\工单\\template\\das-html2pdf.htm","E:\\JAVA\\workspace\\湖南谷歌地图项目\\工单\\template\\result.pdf","E:\\JAVA\\workspace\\湖南谷歌地图项目\\工单\\template\\");
    
    	String htmlStr = HtmlGenerator.generate("freemarkers/das-html2pdf.htm", data);
    	String outputFile = "D:\\sample.pdf";
		OutputStream out = new FileOutputStream(outputFile);
		PdfGenerator.generatePlus(htmlStr, out);
    
    }
}
