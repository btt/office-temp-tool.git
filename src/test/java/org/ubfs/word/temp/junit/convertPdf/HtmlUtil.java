package org.ubfs.word.temp.junit.convertPdf;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.BaseFont;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.util.HtmlUtils;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.*;

/**
 * HTML 工具类
 */
public class HtmlUtil {

    /**
     * 获取HTML内的文本，不包含标签
     *
     * @param html HTML 代码
     */
    public static String getInnerText(String html) {
        if (StringUtils.isNotBlank(html)) {
            //去掉 html 的标签
            String content = html.replaceAll("</?[^>]+>", "");
            // 将多个空格合并成一个空格
            content = content.replaceAll("(&nbsp;)+", "&nbsp;");
            // 反向转义字符
            content = org.springframework.web.util.HtmlUtils.htmlUnescape(content);
            return content.trim();
        }
        return "";
    }

    public static void htmlToPdf(String content,String path) throws DocumentException, FileNotFoundException {

        ITextRenderer renderer = new ITextRenderer();
        ITextFontResolver fontResolver = renderer.getFontResolver();
        try {
            //设置字体，否则不支持中文,在html中使用字体，html{ font-family: SimSun;}
            fontResolver.addFont("templates/SIMSUNB.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        renderer.setDocumentFromString(content);
        renderer.layout();
        renderer.createPDF(new FileOutputStream(new File(path)));
    }

    public static void createPDFFromHtml(String html, String path) throws Exception {

        ITextRenderer renderer = new ITextRenderer();
//        OutputStream out = new ByteArrayOutputStream();

        // 设置 css中 的字体样式（暂时仅支持宋体和黑体） 必须，不然中文不显示
        renderer.getFontResolver().addFont("templates/simsun.ttc", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        // 把html代码传入渲染器中
        renderer.setDocumentFromString(html);

//            // 设置模板中的图片路径 （这里的images在resources目录下） 模板中img标签src路径需要相对路径加图片名 如<img src="images/xh.jpg"/>
//            String url = PDFTemplateUtil.class.getClassLoader().getResource("images").toURI().toString();
//            renderer.getSharedContext().setBaseURL(url);
        renderer.layout();

//        renderer.createPDF(out, false);
        renderer.createPDF(new FileOutputStream(new File(path)), false);
        renderer.finishPDF();
//        out.flush();

//        ByteArrayOutputStream byteArrayOutputStream = (ByteArrayOutputStream) out;

//        return byteArrayOutputStream;
    }

}
