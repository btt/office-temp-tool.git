package org.ubfs.word.temp.junit.execlparse;

import lombok.Data;
import org.ubfs.word.temp.annoation.ExcelField;
import org.ubfs.word.temp.annoation.ExeclTitle;

import java.io.Serializable;
import java.util.Date;

@Data
public class FeeTermDict implements Serializable {

    @ExeclTitle("类别号")
    private String type;

    @ExeclTitle("编码")
    private String code;

    @ExeclTitle("名称")
    private String name;

    private Date createTime;
    private Date updateTime;
    
    


}
