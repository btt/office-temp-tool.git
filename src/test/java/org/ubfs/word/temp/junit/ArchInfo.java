package org.ubfs.word.temp.junit;

import org.ubfs.word.temp.beans.ExcelModel;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ArchInfo {
	
    private ExcelModel parentLabelName;
	
	private String labelName;

	private String fileName;

	private String pageNum;
	
	private String remark;
	
	private String uploadDate;
	
	private String recieveUser;
	
	private String order;


}
