package org.ubfs.word.temp.junit;

import org.junit.Test;
import org.ubfs.word.temp.beans.ImageInf;
import org.ubfs.word.temp.service.imple.TableLoopReplaceHandle;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordTester {

	
	private void buildListData(ReportDataInfo reportDataInfo) {
		// TODO Auto-generated method stub
		List<TableInfo> tableList1 = new ArrayList<TableInfo>();
		for(int i=0;i<10;i++) {
			TableInfo tableInfo = new TableInfo();
			tableInfo.setName("张" + i);
			tableInfo.setAddress("福田保税区");
			tableInfo.setAge("18");
			tableInfo.setTelNo("1888885"+i+"278");
			tableList1.add(tableInfo);
		}
		reportDataInfo.setTableList1(tableList1);
		
		
		List<UserInfo> tableList2 = new ArrayList<UserInfo>();
		for(int i=0;i<10;i++) {
			UserInfo tableInfo = new UserInfo();
			tableInfo.setWorker("CEO");
			tableInfo.setLike("女");
			tableInfo.setWorkYear(5+i);
			tableInfo.setSex("男");
			tableList2.add(tableInfo);
		}
		reportDataInfo.setTableList2(tableList2);
	}
	
	@Test
	public  static void main(String[] args) {
		ReportDataInfo reportDataInfo = new ReportDataInfo();
		WordTester wordTester = new WordTester();
		reportDataInfo.setYear("2019");//文本
		reportDataInfo.setMonth("10");//文本
		reportDataInfo.setImage1(new ImageInf(400,100,"D:\\Test\\file\\timg.jpg")); //图片
		reportDataInfo.setImage2(new ImageInf(400,100,"D:\\Test\\file\\ss.jpg"));//图片
		reportDataInfo.setTempPath("D:\\Test\\file\\test3.docx");    //word 模板路径
		reportDataInfo.setOutPath("D:\\Test\\file\\testFolder\\testResult.docx"); //替换后word的导出路径
		//word替换工具的实例，该类继承自AbstractWordTemple，具体细节见《Word模板工具类API.docx》
		wordTester.buildListData(reportDataInfo);
		TableLoopReplaceHandle wordUtil = new TableLoopReplaceHandle();
		//调用主方法执行报表数据导出到word
		wordUtil.findLabelAndReplace(reportDataInfo);
	}

	@Test
	public void checkTextFormat() {
		String concatText = "}{image3}";
		Pattern r1 = Pattern.compile("(\\{[^\\}]*\\})");
		Pattern r2 = Pattern.compile("\\{");
		Matcher m1 = r1.matcher(concatText);
		Matcher m2 = r2.matcher(concatText);
		int successCount = 0;
		int count = 0;
		while(m1.find()) {
			successCount ++ ;
		}
		while(m2.find()) {
			count ++ ;
		}
		boolean is = count > successCount ? false : true;
		
		System.out.println(is);
	}
	@Test
	public void testMethod2() {
	  String ss = "}{112} {222";
      Pattern r = Pattern.compile("(\\{[^\\}]*\\})");
      
      Pattern r2 = Pattern.compile("\\{");
    
      Matcher m = r2.matcher(ss);
      Matcher m2 = r.matcher(ss);
      int length = 0;
      while(m.find()) {
    	  length ++ ;
      }
      while(m2.find()) {
    	  System.out.println(m2.group());
      }
      
      System.out.println(length);
	}

}
