package org.ubfs.word.temp.junit;

import java.util.List;

import org.ubfs.word.temp.annoation.ExcelField;
import org.ubfs.word.temp.beans.BaseFileTemp;
import org.ubfs.word.temp.constant.ExcelType;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class ExportTest extends BaseFileTemp{
	
	@ExcelField
	private String organName;
	@ExcelField
	private String partYear;
	
	@ExcelField(type= ExcelType.LIST,startRow=1)
	private List<ArchInfo> archInfoList;
	
	

}
