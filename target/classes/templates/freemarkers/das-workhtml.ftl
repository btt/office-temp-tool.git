<!DOCTYPE html>
<!-- saved from url=(0049)file:///C:/Users/wisdom/Desktop/das-html2pdf.html -->
<html lang="en">
<head>
    <title>das-html2pdf</title>
    <style type="text/css">
        .rw-01 {
            width: 100%;
            display: flex;
            justify-content: center;
            flex-flow: row
        }

        .rw-01 .rw-01-001 {
            display: flex;
            justify-content: flex-start;
            align-items: flex-start;
            width: 65px;
            position: absolute;
            left: .5cm
        }

        .rw-01 .rw-01-002 {
            width: 100%;
            display: flex;
            justify-content: flex-start;
            align-items: center;
            flex-flow: column;
            font-size: 16px
        }

        .rw-01 .rw-01-002 font {
            font-weight: 700;
            font-size: 14px
        }

        span {
            border-bottom: 1px solid #000
        }

        p {
            line-height: 1.5
        }

        @page {
            size: 210mm 297mm;
            margin-bottom: 1cm;
            padding: 1em;

            @top-right {
                content: "页眉中间位置";
                font-family: SimSun;
                font-size: 15px;
                color: #000
            }

            @bottom-center {
                content: "页脚中间位置";
                font-family: SimSun;
                font-size: 15px;
                color: #000
            }

            @bottom-right {
                content: "第"counter(page) "页 共"counter(pages) "页";
                font-family: SimSun;
                font-size: 15px;
                color: #000
            }
        }

        #pagenumber:before {
            content: counter(page)
        }

        #pagecount:before {
            content: counter(pages)
        }
    </style>
</head>
<body>
<div data-v-bda0db0c="" id="app" style="font-family: SimSun;">
    <div data-v-405497ab="" data-v-bda0db0c="">
        <div data-v-bda0db0c="" data-v-405497ab="" class="rw-01">
            <div data-v-bda0db0c="" data-v-405497ab="" class="rw-01-001"></div>
            <div data-v-bda0db0c="" data-v-405497ab="" class="rw-01-002">
                <font data-v-bda0db0c="" data-v-405497ab="">Contract No. 12/HY/2019</font>
                <font data-v-bda0db0c="" data-v-405497ab="">Highways Department Term Contract</font>
                <font data-v-bda0db0c="" data-v-405497ab="">(Management &amp; Maintenance of Roads in</font>
                <font data-v-bda0db0c="" data-v-405497ab="" class="span_border">Kowloon West Excluding
                    Expressways and High Speed Roads 2020-2026)</font> <span data-v-bda0db0c=""
                                                                             data-v-405497ab="" class="span_border"
                                                                             style="width: 100%; display: flex; flex-flow: column; text-align: center; font-weight: bold; margin-top: 5px;">
							<font data-v-bda0db0c="" data-v-405497ab="">NOTIFICATION FORM OF DEFECT FOR ROAD MAINTENANCE
								WORKS</font>
							<font data-v-bda0db0c="" data-v-405497ab="" style="font-style: italic;">(ROUTINE/DETAILED *
								INSPECTION)</font>
						</span>
            </div>
        </div>
    </div>
</div>
</body>
</html>
